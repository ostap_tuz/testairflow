from datetime import timedelta

from airflow import DAG

from airflow.contrib.operators.kubernetes_pod_operator import KubernetesPodOperator
from airflow.contrib.kubernetes.volume import Volume
from airflow.contrib.kubernetes.volume_mount import VolumeMount
from airflow.utils.dates import days_ago


default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': days_ago(1),
    'max_active_runs': 1
}

dag = DAG(
    'ETL-2020',
    default_args=default_args,
    schedule_interval=timedelta(minutes=30),
)

kafka_creation_volume_mount = VolumeMount('kafka-topic-volume',
                            mount_path='/usr/kafka/configs',
                            sub_path=None,
                            read_only=True)
kafka_creation_volume_config= {
    'configMap':
      {
        'name': 'kafka-topic-config'
      }
    }
kafka_topic_creation_volume = Volume(name='kafka-topic-volume', configs=kafka_creation_volume_config)

kafka_topic_creation = KubernetesPodOperator(namespace='default',
                          image="dmytroholiash/kafka-topic-creation:1.0",
                          arguments=["/usr/kafka/configs/topicConfig.yml", "/usr/kafka/configs/connection.properties"],
                          labels={"foo": "bar"},
                          volumes=[kafka_topic_creation_volume],
                          volume_mounts=[kafka_creation_volume_mount],
                          name="kafka_topic_creation",
                          task_id="kafka_topic_creation",
                          dag=dag
                          )

# --------------------------------------------------------------------------------------------------
stream_to_kafka_volume_mount = VolumeMount('stream-to-kafka-volume',
                            mount_path='/stream/configs',
                            sub_path=None,
                            read_only=True)
stream_to_kafka_volume_config= {
    'persistentVolumeClaim':
      {
        'claimName': 'stream-to-kafka-volume'
      }
    }
stream_to_kafka_volume = Volume(name='stream-to-kafka-volume', configs=stream_to_kafka_volume_config)

stream_to_kafka = KubernetesPodOperator(namespace='default',
                          image="dmytroholiash/stream-to-kafka:1.0",
                          arguments=["/stream/configs/conf.yml"],
                          labels={"foo": "bar"},
                          volumes=[stream_to_kafka_volume],
                          volume_mounts=[stream_to_kafka_volume_mount],
                          name="stream_to_kafka",
                          task_id="stream_to_kafka",
                          get_logs=True,
                          dag=dag
                          )

# --------------------------------------------------------------------------------------------------
api_to_kafka_volume_mount = VolumeMount('api-to-kafka-volume',
                            mount_path='/usr/kafka/configs',
                            sub_path=None,
                            read_only=True)
api_to_kafka_volume_config= {
    'persistentVolumeClaim':
      {
        'claimName': 'api-to-kafka-volume'
      }
    }
api_to_kafka_volume = Volume(name='api-to-kafka-volume', configs=api_to_kafka_volume_config)

api_to_kafka = KubernetesPodOperator(namespace='default',
                          image="davidrimskiy/api-to-kafka",
                          arguments=["/usr/kafka/configs/api_kafka_config.yml"],
                          labels={"foo": "bar"},
                          volumes=[api_to_kafka_volume],
                          volume_mounts=[api_to_kafka_volume_mount],
                          name="api_to_kafka",
                          task_id="api_to_kafka",
                          get_logs=True,
                          dag=dag
                          )

# --------------------------------------------------------------------------------------------------
forecast_api_volume_mount = VolumeMount('forecast-api-volume',
                            mount_path='/forecast/configs',
                            sub_path=None,
                            read_only=True)
forecast_api_volume_config= {
    'persistentVolumeClaim':
      {
        'claimName': 'forecast-api-volume'
      }
    }
forecast_api_volume = Volume(name='forecast-api-volume', configs=forecast_api_volume_config)

forecast_api = KubernetesPodOperator(namespace='default',
                          image="dmytroholiash/forecast:1.0",
                          arguments=["/forecast/configs/config.yaml"],
                          labels={"foo": "bar"},
                          volumes=[forecast_api_volume],
                          volume_mounts=[forecast_api_volume_mount],
                          name="forecast_api",
                          task_id="forecast_api",
                          get_logs=True,
                          dag=dag
                          )

# --------------------------------------------------------------------------------------------------
weather_volume_mount = VolumeMount('weather-volume',
                            mount_path='/forecast/configs',
                            sub_path=None,
                            read_only=True)
weather_volume_config= {
    'persistentVolumeClaim':
      {
        'claimName': 'weather-volume'
      }
    }
weather_volume = Volume(name='weather-volume', configs=weather_volume_config)

weather = KubernetesPodOperator(namespace='default',
                          image="dmytroholiash/weather-spark-image:latest",
                          arguments=["/opt/spark/work-dir/weather/configs/conf.yml"],
                          labels={"foo": "bar"},
                          volumes=[weather_volume],
                          volume_mounts=[weather_volume_mount],
                          name="weather",
                          task_id="weather",
                          get_logs=True,
                          dag=dag
                          )

kafka_topic_creation >> stream_to_kafka
stream_to_kafka.set_downstream(api_to_kafka)
stream_to_kafka.set_downstream(forecast_api)
stream_to_kafka.set_downstream(weather)
